import time
import grovepi
import sys
from picamera import PiCamera
import os
import json
import cv2
import math

 
# Connect the Grove PIR Motion Sensor to digital port D8
# SIG,NC,VCC,GND
pir_sensor = 8

grovepi.pinMode(pir_sensor,"INPUT")


if __name__=="__main__":   
    while True:
        try:
            time.sleep(1)
            if grovepi.digitalRead(pir_sensor):
                y = 'Intruder Detected'
                print(y)
                
            else:
                time.sleep(3)
                y = 'idle'
                print(y)
            time.sleep(0.2)    
        except KeyboardInterrupt:
            print ("Terminated.")
            os._exit(0)

 
