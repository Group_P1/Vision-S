import time
import grovepi
import sys
from picamera import PiCamera
import os
import json
import cv2
import math

 
# Connect the Grove PIR Motion Sensor to digital port D8
# SIG,NC,VCC,GND
pir_sensor = 8
sensor = 4
buzzer = 3
blue = 0    # The Blue colored sensor.

grovepi.pinMode(pir_sensor,"INPUT")

classNames = []
classFile = "/home/pi/Desktop/Object_Detection_Files/coco.names"
with open(classFile,"rt") as f:
    classNames = f.read().rstrip("\n").split("\n")

configPath = "/home/pi/Desktop/Object_Detection_Files/ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt"
weightsPath = "/home/pi/Desktop/Object_Detection_Files/frozen_inference_graph.pb"

net = cv2.dnn_DetectionModel(weightsPath,configPath)
net.setInputSize(320,320)
net.setInputScale(1.0/ 127.5)
net.setInputMean((127.5, 127.5, 127.5))
net.setInputSwapRB(True)


# if sys.platform == 'uwp':
#     import winrt_smbus as smbus
#     bus = smbus.SMBus(1)
# else:
#     import smbus
#     import RPi.GPIO as GPIO
#     rev = GPIO.RPI_REVISION
#     if rev == 2 or rev == 3:
#         bus = smbus.SMBus(1)
#     else:
#         bus = smbus.SMBus(0)
#         
#         
# DISPLAY_RGB_ADDR = 0x62
# DISPLAY_TEXT_ADDR = 0x3e

def getObjects(img, thres, nms, draw=True, objects=[]):
    classIds, confs, bbox = net.detect(img,confThreshold=thres,nmsThreshold=nms)
    #print(classIds,bbox)
    if len(objects) == 0: objects = classNames
    objectInfo =[]
    if len(classIds) != 0:
        for classId, confidence,box in zip(classIds.flatten(),confs.flatten(),bbox): #set the parameters to draw boxes to detect objects
            className = classNames[classId - 1]
            if className in objects:
                objectInfo.append([box,className])
                if (draw):
                    cv2.rectangle(img,box,color=(0,255,0),thickness=2)
                    cv2.putText(img,classNames[classId-1].upper(),(box[0]+10,box[1]+30),
                    cv2.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)
                    cv2.putText(img,str(round(confidence*100,2)),(box[0]+200,box[1]+30),
                    cv2.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)

    return img,objectInfo

# def setRGB(r,g,b):
#     bus.write_byte_data(DISPLAY_RGB_ADDR,0,0)
#     bus.write_byte_data(DISPLAY_RGB_ADDR,1,0)
#     bus.write_byte_data(DISPLAY_RGB_ADDR,0x08,0xaa)
#     bus.write_byte_data(DISPLAY_RGB_ADDR,4,r)
#     bus.write_byte_data(DISPLAY_RGB_ADDR,3,g)
#     bus.write_byte_data(DISPLAY_RGB_ADDR,2,b)
#         
# def textCommand(cmd):
#     bus.write_byte_data(DISPLAY_TEXT_ADDR,0x80,cmd)       
# 
# def setText(text):
#     textCommand(0x01) # clear display
#     time.sleep(.05)
#     textCommand(0x08 | 0x04) # display on, no cursor
#     textCommand(0x28) # 2 lines
#     time.sleep(.05)
#     count = 0
#     row = 0
#     for c in text:
#         if c == '\n' or count == 16:
#             count = 0
#             row += 1
#             if row == 2:
#                 break
#             textCommand(0xc0)
#             if c == '\n':
#                 continue
#         count += 1
#         bus.write_byte_data(DISPLAY_TEXT_ADDR,0x40,ord(c))
#         

 
#camera = PiCamera()
if __name__=="__main__":
#capture video footage
    cap = cv2.VideoCapture(0) #select the piCamera
    cap.set(3,840)#set the dimensions of the display window
    cap.set(2,160)
    
    
    
    while True:
        try:
            [temp,humidity] = grovepi.dht(sensor,blue) #read the temperature and humidity sensor data
            line = [] 
            if math.isnan(temp) == False and math.isnan(humidity) == False: #check if the values that are read are valid numbers
                if temp > 50:
                    grovepi.digitalWrite(buzzer,1) #if the temperature value is greater than 50, then the buzzer should be sounded
                    time.sleep(1)
                n = [temp,humidity]
                line.extend(n)       
            else: 
                print("No data.")
            
            
            
            
            #distance = grovepi.ultrasonicRead(ultrasonic_ranger)
            #sys.stdout.write("The distance is \r%d" %distance)
            #if distance < 10:
                #success, img = cap.read()
                #objectInfo = getObjects(img,0.55,0.3,objects=['person'])#detect objects only if the accuracy is greater than 55%, detect only humans using the camera 
                #cv2.imshow("Output",img)
                #cv2.waitKey(1)
                
            success, img = cap.read()
            result, objectInfo = getObjects(img,0.40,0.3,objects=['person','dog','cat'])#detect objects only if the accuracy is greater than 55%, detect only humans using the camera 
            #result, objectInfo = getObjects(img,0.45,0.2)
            try:
                if objectInfo[0][1] == 'person' or objectInfo[0][1] == 'dog' or  objectInfo[0][1] == 'cat'  :#check whether there is an object detected and whether if it is a person a dog or a cat
                    line.append(objectInfo[0][1])
                    #file = open("/var/www/html/data.txt","w")#overwrite the value in the file so that the temperature can be updated
                    #file.write(str(line))
                    #file.close()
                else:
                    line.remove(person)
                    
                    
                print(objectInfo[0][1])
            except IndexError as e:
                print('No object found')
            file = open("/var/www/html/data.txt","w")#overwrite the value in the file so that the temperature can be updated
            file.write(str(line))
            file.close()
            cv2.imshow("Output",img)
            cv2.waitKey(1)

            
            
            #if cv2.waitKey(1) & 0xff == ord('q'):
                #break
            time.sleep(1)
            if grovepi.digitalRead(pir_sensor):#The PIR motion sensor checks if movement is being detected
                #time.sleep(5)
                y = 'Intruder Detected'
                #x = json.dumps({'Alert': y})
                #setText(x)
                #time.sleep(10)
                #camera.start_preview()
                #time.sleep(5)
                
                
            else:
                #time.sleep(3)
                y = 'idle'
                x = json.dumps({'Alert': y})
                #setText(x)
                #camera.stop_preview()
            #time.sleep(0.2)
                
        except KeyboardInterrupt:
            print ("Terminated.")
            os._exit(0)

 
