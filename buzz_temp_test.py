import time
import grovepi
import sys
from picamera import PiCamera
import os
import json
import cv2
import math

 
# Connect the Grove PIR Motion Sensor to digital port D8
# SIG,NC,VCC,GND
pir_sensor = 8
sensor = 4
buzzer = 3
blue = 0    # The Blue colored sensor.

if __name__=="__main__":   
    while True:
        try:
            [temp,humidity] = grovepi.dht(sensor,blue)
            line = [] 
            if math.isnan(temp) == False and math.isnan(humidity) == False:
                if temp > 50:
                    grovepi.digitalWrite(buzzer,1)
                    time.sleep(1)
                n = [temp,humidity]
                line.extend(n)       
            else: 
                print("No data.")
            file = open("/var/www/html/data.txt","w")#overwrite the value in the file so that the temperature can be updated
            file.write(str(line))
            file.close()
        except KeyboardInterrupt:
            print ("Terminated.")
            os._exit(0)

 
