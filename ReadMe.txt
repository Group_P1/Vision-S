Worked on the following parts:
- Pir Motoin Sensor (pir_motion_test.py): to track motoin of any object.
- Pi Camera (cv2_tst_camera.py): to access the vision for tracking object.
- Temperature & Hunidity Sensor and Buzzer (buzz_temp_test.py): to track the temperature and humidity and reaching or surpassing a particular temperature will alert with a buzzer.
- Object Detection (OD_test.py): to track the object from the video using Pi Camera and tracking the object as categories like Human/Dog/Cat with the percentage of accuracy.
- Web Interface (tempApp.php): It displays the temperature and humidity from the sensors and after tracking the object and labelling it as Human/Dog/Cat, it displays on to the web interface. 